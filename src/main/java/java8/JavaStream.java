package java8;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JavaStream {

	public static void main(String[] args) throws IOException {

		List<String> strings = Arrays.asList("Shashank", "", "Shekhar", "Java", "Stream", "");

		List<String> filtered = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());
		String messages = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.joining("jjj"));

		int count = (int) strings.stream().filter(str -> str.isEmpty()).count();
		System.out.println(messages.split("jjj").length);
		System.out.println(filtered);

		// The �map� method is used to map each element to its corresponding result. The
		// following code segment prints unique squares of numbers using map.
		List<Integer> integernumbers = Arrays.asList(1, 2, 3, 4, 5, 7, 1, 9, 16, 25);
		List<Integer> nums = integernumbers.stream().map(i -> i * i).distinct().collect(Collectors.toList());
		System.out.println(nums);

		// Printing RandomNunbers

		Random random = new Random();
		random.ints().limit(5).forEach(System.out::println);

		// Map File
		Stream<String> rows = Files.lines(Paths.get("Data.csv"));
		Map<String, Integer> map = new HashMap<>();

		map = rows.map(x -> x.split(",")).collect(Collectors.toMap(x -> x[0], x -> Integer.parseInt(x[1])));

		
		for(String key : map.keySet()) {
			System.out.println(key +"    "+map.get(key));
		}
	}
}
