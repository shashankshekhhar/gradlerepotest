package jdbcConnections;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Connections2 {

	@Test
	public void connections() throws SQLException, JsonGenerationException, JsonMappingException, IOException {

		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ClassicModels", "root", "root");

		String query = "select * from CustomerInfo";
		Statement st = con.createStatement();
		ResultSet rest = st.executeQuery(query);
		
		ArrayList<CustomerDetails> arr= new ArrayList();
		HashMap<String,ArrayList> map=new HashMap();

		while (rest.next()) {
			CustomerDetails cu = new CustomerDetails();
			String bookName = rest.getString("BookName");
			String purchasedDate = rest.getString("PurchasedDate");
			int amount = rest.getInt("Amount");
			String location = rest.getString("Location");
			cu.setAmount(amount);
			cu.setBookName(bookName);
			cu.setLocation(location);
			cu.setPurchasedDate(purchasedDate);
			System.out.println(bookName);
			arr.add(cu);
		
		}
		map.put("data", arr);
		/*for(CustomerDetails cust : arr) {
			System.out.println(cust.bookName);
			
		}*/
		
		//For creating sepratefiles
		for(int i=0;i<arr.size();i++) {
			
		File  file= new File("D:\\ApiTestingFramework\\GradleProject1\\"+i+".json");
		ObjectMapper om= new ObjectMapper();
		om.writeValue(file, arr.get(i));
		System.out.println(arr.get(i));
		}
		
		//For
		File  file= new File("D:\\ApiTestingFramework\\GradleProject1\\"+"APi"+".json");
		ObjectMapper om= new ObjectMapper();
		om.writeValue(file,map);
		
	for(Map.Entry<String,ArrayList> entry : map.entrySet()) {

		System.out.println(entry.getKey()+entry.getValue().get(4));
	}
		con.close();
	}
}
