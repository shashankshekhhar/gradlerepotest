package jdbcConnections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

public class Connections {

	@Test
	public void connections() throws SQLException {

		Connection con = DriverManager
	.getConnection("jdbc:mysql://localhost:3306/ClassicModels","root","root");

		String query = "select * from CustomerInfo";
		Statement st = con.createStatement();
		ResultSet rest = st.executeQuery(query);

		while (rest.next()) {

			String bookName = rest.getString("BookName");
			String purchasedDate = rest.getString("PurchasedDate");

			int amount = rest.getInt("Amount");
			String location = rest.getString("Location");

			System.out.println(bookName + "  " + amount);
		}
con.close();
	}
}
