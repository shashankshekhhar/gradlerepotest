package jdbcConnections;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Jsonchema {

	static String SchemaName="D:\\ApiTestingFramework\\GradleProject1\\src\\test\\java\\jdbcConnections\\schemato.json";
	
	

	public static JSONObject getRootNode(String SchemaName) throws UnsupportedEncodingException, IOException, ParseException {
		JSONObject jsonSchema;
		InputStream in=Jsonchema.class.getClassLoader().getResourceAsStream(SchemaName);
		Object obj=new JSONParser().parse(new InputStreamReader(in,"UTF-8"));
		jsonSchema=(JSONObject) obj;
		return (JSONObject)((JSONObject)jsonSchema.get(jsonSchema.keySet().iterator().next().toString())).get("properties");
				

	}
	
	
	public static void main(String[] args) throws UnsupportedEncodingException, IOException, ParseException {
		
		System.out.println(getRootNode(SchemaName));
		
	}
	
	

}
